![CHIP-8](assets/icon-above-font.png)

# Kotlin CHIP-8
A CHIP-8 interpreter written in Kotlin.